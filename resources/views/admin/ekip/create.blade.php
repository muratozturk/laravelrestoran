@extends ('admin/template')

@section('icerik')


    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                    <h5> Ekibe Kişi Ekle</h5>
                </div>



                <div class="widget-content nopadding">
                    {!! Form::open(['route'=>'ekip.store','method'=>'POST','class'=>'form-horizontal','files'=>'true']) !!}

                    <div class="control-group">
                        <label class="control-label"> Adı ve Soyadı</label>
                        <div class="controls">
                            <input type="text" class="span11" name="ad" required />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"> Görevi</label>
                        <div class="controls">
                            <input type="text" class="span11" name="gorevi" required />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"> Fotoğraf</label>
                        <div class="controls">
                            <input type="file" name="resim"  class="span11" required />
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success">Kişi Ekle</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>

    </div>

@endsection

@section('css')

@endsection


@section('js')

@endsection