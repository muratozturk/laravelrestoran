@extends ('admin/template')

@section('icerik')


    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                    <h5> Ekibe Kişi Ekle</h5>
                </div>



                <div class="widget-content nopadding">
                    {!! Form::model($ekip,['route'=>['ekip.update',$ekip->id],'method'=>'PUT','class'=>'form-horizontal','files'=>'true']) !!}

                    <div class="control-group">
                        <label class="control-label"> Adı ve Soyadı</label>
                        <div class="controls">
                            <input type="text" class="span11" value="{{$ekip->ad}}" name="ad" required />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"> Görevi</label>
                        <div class="controls">
                            <input type="text" class="span11" value="{{$ekip->gorevi}}" name="gorevi" required />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"> Fotoğraf</label>
                        <div class="controls">
                            <input type="file" name="resim"  class="span11" />
                        </div>
                        <img width="200" src="/{{$ekip->resim}}">
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success">Kişi Güncelle</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>

    </div>

@endsection

@section('css')

@endsection

@section('js')

@endsection