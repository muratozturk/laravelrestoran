@extends ('admin/template')

@section('icerik')


    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                    <h5>Kategori:{{$kategori->restoranmenu_kategori}} </h5>
                </div>



                <div class="widget-content nopadding">
                    {!! Form::model($kategori,['route'=>['restoranmenukategori.update',$kategori->id],'method'=>'PUT','class'=>'form-horizontal','files'=>'true']) !!}



                    <div class="control-group">
                        <label class="control-label">Kategori Adı</label>
                        <div class="controls">
                            <input type="text" class="span11" name="restoranmenu_kategori"  value="{{$kategori->restoranmenu_kategori}}" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Kategori Açıklama</label>
                        <div class="controls">
                            <input type="text" class="span11" name="restoranmenu_kategori_aciklama" value="{{$kategori->restoranmenu_kategori_aciklama}}"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Kategori Fotoğrafı</label>
                        <div class="controls">
                            <input type="file" class="span11" name="resim"/>
                        </div>
                        <label class="control-label">
                        <img width="200" src="/{{$kategori->fotograf}}">
                        </label>
                    </div>


                    <div class="form-actions">
                        <button type="submit" class="btn btn-success">Kategori Güncelle</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>

    </div>

@endsection

@section('css')

@endsection

@section('js')

@endsection