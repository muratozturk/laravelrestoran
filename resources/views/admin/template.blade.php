 <!DOCTYPE html>
<html lang="en">
<head>
    <title>{{$ayar->site_adi}}</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="/admin/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/admin/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="/admin/css/fullcalendar.css" />
    <link rel="stylesheet" href="/admin/css/matrix-style.css" />
    <link rel="stylesheet" href="/admin/css/matrix-media.css" />
    <link href="/admin/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="/admin/css/jquery.gritter.css" />
    @yield('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

</head>
<body>

<!--Header-part-->
<div id="header">
    <h1><a href="/">Tag </a></h1>
</div>
<!--close-Header-part-->


<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
    <ul class="nav">
        <li  class="dropdown" id="profile-messages" ><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text"> Merhaba, {{Auth::user()->name}} </span><b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a target="_blank" href="{{route('anasayfa')}}"><i class="icon-user"></i>Web Sitesi</a></li>
                <li><a href="{{route('kullanici.duzenle',Auth::user()->id)}}"><i class="icon-user"></i> Profilim</a></li>

                <li class="divider"></li>
                <li><a href="{{route('yonetim.cikis')}}"><i class="icon-key"></i> Çıkış</a></li>
            </ul>
        </li>

    </ul>
</div>
<div id="sidebar">
    <ul>
        <li ><a href="{{route('yonetim.index')}}"><i class="icon icon-home"></i> <span>Panel Anasayfa</span></a> </li>

        <li class="submenu" > <a href="#"><i class="icon icon-wrench"></i> <span>Ayarlar </span><span class="label label-important">2</span></a>
            <ul>
                <li><a href="{{route('ayarlar.index')}}">Site Ayarları</a></li>
                <li><a href="{{route('anasayfaayar.index')}}">Anasayfa Ayarları</a></li>
             </ul>
         </li>

        <li> <a href="{{route('sayfalar.index')}}"><i class="icon icon-paper-clip"></i> <span>Sayfa Yönetimi </span></a> </li>
        {{--<li> <a href="{{route('hizmetler.index')}}"><i class="icon icon-star"></i> <span>Hizmet Yönetimi </span></a>--}} </li>
        <li> <a href="{{route('ekip.index')}}"><i class="icon icon-group"></i> <span>Ekip Yönetimi </span></a> </li>
        <li> <a href="{{route('sliderlar.index')}}"><i class="icon icon-film"></i> <span>Slider Yönetimi </span></a> </li>
       {{-- <li> <a href="{{route('referanslar.index')}}"><i class="icon icon-briefcase"></i> <span>Referans Yönetimi </span></a> </li>--}}

        <li class="submenu" > <a href="#"><i class="icon icon-book"></i> <span>İçerik Yönetimi </span><span class="label label-important">3</span></a>
             <ul>
                    <li><a href="{{route('yazilar.create')}}">Yeni Yazı Ekle</a></li>
                    <li><a href="{{route('yazilar.index')}}">Tüm Yazılar</a></li>
                    <li><a href="{{route('kategori.index')}}">Kategori Yönetimi</a></li>
             </ul>
        </li>

        <li class="submenu" > <a href="#"><i class="icon icon-list"></i> <span>Restoran Menü Yönetimi </span><span class="label label-important">3</span></a>
            <ul>
                <li><a href="{{route('restoranmenu.create')}}">Yeni Menü Ekle</a></li>
                <li><a href="{{route('restoranmenu.index')}}">Tüm Menüler</a></li>
                <li><a href="{{route('restoranmenukategori.index')}}">Kategori Yönetimi</a></li>

            </ul>
        </li>
        <li> <a href="{{route('kullanicilar.index')}}"><i class="icon icon-user-md"></i> <span>Kullanıcı Yönetimi </span></a></li>
        <li> <a href="{{route('menuler.index')}}"><i class="icon icon-group"></i> <span>Menü Yönetimi</span></a></li>
        <li> <a href="{{route('galeri')}}"><i class="icon icon-camera"></i> <span>Foto Galeri </span></a> </li>
        <li> <a href="{{route('yorumlar.index')}}"><i class="icon icon-comments"></i> <span>Yorumlar </span></a> </li>
        <li class="bg_lr"> <a href="{{route('yonetim.cikis')}}"><i style="color: white" class="icon icon-off"></i> <span style="color: white">Çıkış </span></a> </li>



    </ul>
</div>
<!--sidebar-menu-->

<!--main-container-part-->
<div id="content">


    <!--Action boxes-->
    <div class="container-fluid">

        @yield('icerik')

        <!--End-Action boxes-->


    </div>

</div>
<div class="row-fluid">
    <div id="footer" style="color: #ffffff;" class="span12"> Tag Dijital Ajans Penel V1.0</div>
</div>
<script src="/admin/js/excanvas.min.js"></script>
<script src="/admin/js/jquery.min.js"></script>
<script src="/admin/js/jquery.ui.custom.js"></script>
<script src="/admin/js/bootstrap.min.js"></script>
<script src="/admin/js/jquery.flot.min.js"></script>
<script src="/admin/js/jquery.flot.resize.min.js"></script>
<script src="/admin/js/jquery.peity.min.js"></script>
<script src="/admin/js/fullcalendar.min.js"></script>
<script src="/admin/js/matrix.js"></script>
<script src="/admin/js/matrix.dashboard.js"></script>
<script src="/admin/js/jquery.gritter.min.js"></script>
<script src="/admin/js/matrix.interface.js"></script>
<script src="/admin/js/matrix.chat.js"></script>
<script src="/admin/js/jquery.validate.js"></script>
<script src="/admin/js/matrix.form_validation.js"></script>
<script src="/admin/js/jquery.wizard.js"></script>
<script src="/admin/js/jquery.uniform.js"></script>
<script src="/admin/js/select2.min.js"></script>
<script src="/admin/js/matrix.popover.js"></script>
<script src="/admin/js/jquery.dataTables.min.js"></script>
<script src="/admin/js/matrix.tables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>

@include('sweetalert::alert')

@yield('js')


</body>
</html>

