@extends ('admin/template')

@section('icerik')


    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                    <h5>Yorum Düzenle </h5>
                </div>



                <div class="widget-content nopadding">
                    {!! Form::model($yorum,['route'=>['yorumlar.update',$yorum->id],'method'=>'PUT','class'=>'form-horizontal']) !!}



                    <div class="control-group">
                        <label class="control-label">Yorum</label>
                        <div class="controls">
                            <textarea class="span11" name="yorum" required maxlength="190">{{$yorum->yorum}}</textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Ad Soyad</label>
                        <div class="controls">
                            <input type="text" class="span11" value="{{$yorum->ad}}" name="ad"  required/>
                        </div>
                    </div>


                    <div class="form-actions">
                        <button type="submit" class="btn btn-success">Yorumu Güncelle</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>

    </div>

@endsection

@section('css')

@endsection

@section('js')

@endsection