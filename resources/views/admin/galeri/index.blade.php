@extends ('admin/template')

@section('icerik')

    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                    <h5>Galeri</h5>
                </div>
                <div class="widget-content nopadding">
                    {!! Form::open(array('url'=>'yonetim/galeri/uploadFiles','method'=>'POST', 'files'=>true)) !!}
                    <div class="control-group">
                        <label class="control-label">Galeriye Fotograf Ekle </label>
                        <div class="controls">
                            {!! Form::file('images[]', array('multiple'=>true , 'required' =>'required')) !!}
                            {!! Form::submit('Yükle', array('class'=>'btn btn-lg btn-primary col-md-4')) !!}
                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="widget-box">
        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Yüklü Fotoğraflar</h5>
        </div>
        <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
                <thead>
                <tr>
                    <th>Fotoğraf</th>
                    <th width="15%">SEO</th>
                    <th width="5%">Sil</th>
                </tr>
                </thead>
                <tbody>

                @foreach($fotograflar as $foto)

                    <tr class="gradeX">
                        <td><img width="150" height="150" src="/{{$foto->fotograf}}"></td>

                        <td>{{$foto->seo}}</td>

                        </form>
                        {!! Form::model($foto,['route'=>['foto.sil',$foto->id],'method'=>'DELETE']) !!}

                        <td class="center">

                            <button type="submit" onclick="return window.confirm('Silmek istediğinize eminmisiniz?');" class="btn btn-danger btn-mini">Sil</button>

                        </td>

                        {!! Form::close() !!}

                    </tr>

                @endforeach


                </tbody>
            </table>
        </div>
    </div>


@endsection

@section('css')


@endsection

@section('js')

@endsection