@extends ('admin/template')

@section('icerik')

    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                    <h5>Anasayfa Ayarları</h5>
                </div>
                <div class="widget-content nopadding">
                    {!! Form::model($anasayfaayar,['route'=>['anasayfaayar.update',1],'method'=>'PUT','class'=>'form-horizontal']) !!}

                    <div class="control-group">
                        <label class="control-label"> Anasayfa Metin</label>
                        <div class="controls">
                            <textarea cols="100" name="metin">{!! $anasayfaayar->metin !!}</textarea>

                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Anasayfa Yorumlar </label>
                        <div class="controls">
                            <select name="yorumlar" class="span11">

                                @if($anasayfaayar->yorumlar=='evet')
                                <option  value="evet" selected>Göster</option>
                                <option  value="hayir" >Gösterme</option>
                                @else
                                    <option  value="evet" >Göster</option>
                                    <option  value="hayir" selected>Gösterme</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Anasayfa Galeri </label>
                        <div class="controls">
                            <select name="galeri" class="span11">

                                @if($anasayfaayar->galeri=='evet')
                                    <option  value="evet" selected>Göster</option>
                                    <option  value="hayir" >Gösterme</option>
                                @else
                                    <option  value="evet" >Göster</option>
                                    <option  value="hayir" selected>Gösterme</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Anasayfa Blog Haber </label>
                        <div class="controls">
                            <select name="blog" class="span11">

                                @if($anasayfaayar->blog=='evet')
                                    <option  value="evet" selected>Göster</option>
                                    <option  value="hayir" >Gösterme</option>
                                @else
                                    <option  value="evet" >Göster</option>
                                    <option  value="hayir" selected>Gösterme</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Anasayfa Menü </label>
                        <div class="controls">
                            <select name="menu" class="span11">

                                @if($anasayfaayar->menu=='evet')
                                    <option  value="evet" selected>Göster</option>
                                    <option  value="hayir" >Gösterme</option>
                                @else
                                    <option  value="evet" >Göster</option>
                                    <option  value="hayir" selected>Gösterme</option>
                                @endif
                            </select>
                        </div>
                    </div>




                    <div class="form-actions">
                        <button type="submit" class="btn btn-success">Güncelle</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>

    </div>

@endsection

@section('css')

@endsection

@section('js')
    <script src="/admin/tinymce/js/tinymce/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
@endsection