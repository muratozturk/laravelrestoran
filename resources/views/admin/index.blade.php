@extends('admin.template')

@section('icerik')

    <!--Action boxes-->
    <div class="container-fluid">
        <div class="quick-actions_homepage">
            <ul class="quick-actions">
                <li class="bg_lb"> <a href="{{route('sayfalar.create')}}"> <i class="icon-plus-sign"></i> Sayfa Ekle</a> </li>
                <li class="bg_lg"> <a href="{{route('yazilar.create')}}"> <i class="icon-plus-sign"></i>Yeni Haber</a> </li>
                <li class="bg_ly"> <a href="{{route('galeri')}}"> <i class="icon-plus-sign"></i>Galeri </a> </li>
                <li class="bg_lo"> <a href="{{route('ekip.create')}}"> <i class="icon-plus-sign"></i>Yeni Ekip</a> </li>
                <li class="bg_ls"> <a href="{{route('restoranmenu.create')}}"> <i class="icon-plus-sign"></i> Yiyecek Menü Ekle</a> </li>
                <li class="bg_lb"> <a href="{{route('anasayfaayar.index')}}"> <i class="icon-certificate"></i> Anasayfa Ayarları</a> </li>


            </ul>
        </div>
        <!--End-Action boxes-->

        <div class="container-fluid">

        <div class="row-fluid">
            <!--İstatistik-->
            <div class="span6">
            <div class="widget-box">
                <div class="widget-title bg_lg"><span class="icon"><i class="icon-signal"></i></span>
                    <h5>Site İstatistik</h5>
                </div>
                <div class="widget-content" >
                    <div class="row-fluid">

                        <div class="span12">
                            <ul class="site-stats">
                                <li class="bg_lb"> <strong>{{$sayfacount}}</strong> <small>Sayfa</small></li>
                                <li class="bg_ly"> <strong>{{$galericount}}</strong> <small>Fotoğraf</small></li>
                                <li class="bg_lg"> <strong>{{$habercount}}</strong> <small>Haber</small></li>
                                <li class="bg_ls"> <strong>{{$restoranmenucount}}</strong> <small>Restoran Menü</small></li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <!--İstatistik-->

            <!--Son Haberler-->
            <div class="span6">
            <div  class="widget-box">
                <div  class="widget-title bg_lo"  data-toggle="collapse" href="#collapseG3" > <span class="icon icon-paper-clip"> <i class="icon-chevron-down"></i> </span>
                    <h5>  Son Haberler</h5>
                </div>

                @foreach ($lastnews as $haber)
                    <div class="new-update clearfix">
                        <i class="icon-gift"></i>
                        <span class="update-notice">
                        <a title="" href="#"><strong>{{$haber->baslik}} </strong></a>
                        <span>{!! str_limit(strip_tags($haber->icerik ),$limit=200,$end='...') !!}</span> </span>
                    </div>
                @endforeach
            </div>
            </div>
            <!--Son Haberler-->

        </div>
        <div class="row-fluid">
            <!--İstatistik-->
            <div class="span6">
                <div class="widget-box">
                    <div  class="widget-title bg_lo"  data-toggle="collapse" href="#collapseG3" > <span class="icon icon-paper-clip"> <i class="icon-chevron-down"></i> </span>
                        <h5>  Son Yorumlar</h5>
                    </div>

                    @foreach ($yorumlar as $yorum)
                        <div class="new-update clearfix">
                            <i class="icon-gift"></i>
                            <span class="update-notice">
                        <a title="" href="#"><strong>{{$yorum->ad}} </strong></a>
                        <span>{!! str_limit(strip_tags($yorum->yorum ),$limit=200,$end='...') !!}</span> </span>
                        </div>
                    @endforeach
                </div>
            </div>
            <!--İstatistik-->

            <!--Son Haberler-->
            <div class="span6">
                <div  class="widget-box">
                    <div  class="widget-title bg_lo"  data-toggle="collapse" href="#collapseG3" > <span class="icon icon-paper-clip"> <i class="icon-chevron-down"></i> </span>
                        <h5>  Son Haberler</h5>
                    </div>

                    @foreach ($menuler as $menu)
                        <div class="new-update clearfix">
                            <i class="icon-gift"></i>
                            <span class="update-notice">
                        <a title="" href="#"><strong>{{$menu->menuadi}} : Fiyat {{$menu->fiyat}} {{$ayar->parabirimi}}</strong></a>
                        <span>{!! str_limit(strip_tags($menu->menuaciklama ),$limit=200,$end='...') !!}</span> </span>
                        </div>
                    @endforeach
                </div>
            </div>
            <!--Son Haberler-->

        </div>
    </div>
    </div>

@endsection


@section('css')

@endsection

@section('js')

@endsection