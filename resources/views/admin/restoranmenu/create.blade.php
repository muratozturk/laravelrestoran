
@extends('admin/template')
@section('icerik')

    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                    <h5>Menü Ekle</h5>
                </div>

                <div class="widget-content nopadding">
                    {!! Form::open(['route'=>'restoranmenu.store','method'=>'POST','class'=>'form-horizontal','files'=>'true']) !!}

                    <div class="control-group">
                        <label class="control-label">Kategori Seçin</label>
                        <div class="controls">
                            <select name="kategori" class="span11">

                                @foreach($kategoriler as $kategori)

                                    <option value="{{$kategori->id}}">{{$kategori->restoranmenu_kategori}}</option>


                                @endforeach

                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Menü Adı</label>
                        <div class="controls">
                            <input type="text" class="span11" name="menuadi"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Menü Açıklama</label>
                        <div class="controls">
                            <textarea name="menuaciklama" class="span11" required></textarea>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Menü Resmi</label>
                        <div class="controls">
                            <input type="file" class="span11" name="resim"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Menü Fiyatı (TL)</label>
                        <div class="controls">
                            <input width="20" type="number" min="1" max="9999" step="any"  name="fiyat" class="span11" required>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success">İçerik Ekle</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>

    </div>

@endsection

@section('css')

@endsection

@section('js')
    <script src="/admin/tinymce/js/tinymce/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>

@endsection

