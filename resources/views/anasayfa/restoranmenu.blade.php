@extends('anasayfa.template')

@section('icerik')


    <div class="container">
        <div class="row">
            <div class="col-md-12 center">
                <h1 class="mt-xlg mb-sm pt-md">Yiyecek ve <strong>&amp; </strong> İçecekler</h1>
                <p class="font-size-md">Yiyecek ve İçecek Menülerimiz...</p>

                <hr class="custom-divider">
            </div>
        </div>
    </div>

    <section class="section section-default">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="tabs tabs-bottom tabs-center tabs-simple">
                        <ul class="nav nav-tabs">
                            @foreach ($restoranmenukategoriler as $kategori)
                            <li >
                                <a href="#{{$kategori->slug}}" data-toggle="tab">{{$kategori->restoranmenu_kategori}}</a>
                            </li>
                                @endforeach

                        </ul>
                        <div class="tab-content">
                            @foreach ($restoranmenukategoriler as $kategori)
                            <div class="tab-pane active" id="{{$kategori->slug}}">

                                @foreach($kategori->menuleri->chunk(3) as $array)
                                <div class="row">
                                    @foreach ($array as $restoranmenu)
                                    <div class="col-md-4">
                                        <div class="menu-item">
                                            <span class="menu-item-price">{{$restoranmenu->fiyat}} {{$ayar->parabirimi}}</span>
                                            <h4>{{$restoranmenu->menuadi}}</h4>
                                            <p>{{strip_tags($restoranmenu->menuaciklama)}}</p>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                @endforeach

                            </div>
                           @endforeach


                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>







@endsection

@section('css')

@endsection

@section('js')

@endsection