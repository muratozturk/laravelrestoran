@extends('anasayfa.template')

@section('icerik')




    <div class="container">

        <div class="row pt-sm">
            <div class="col-md-12">
                <div class="blog-posts single-post mt-xl">

                    <article class="post post-large blog-single-post">

                        <div class="post-date">
                            <span class="day">{{date('d',strtotime($haber->created_at))}}</span>
                            <span class="month">{{date('m',strtotime($haber->created_at))}}</span>
                        </div>

                        <div class="post-content">

                            <h1 class="mb-md">{{$haber->baslik}}</h1>



                            <img src="/{{$haber->resim}}" class="img-responsive pull-right mb-md mb-xs ml-xl" alt="" style="width: 360px;">

                            <p class="lead">{!! $haber->icerik !!}</p>


                        </div>
                    </article>

                </div>
            </div>
        </div>

    </div>



@endsection

@section('css')

@endsection

@section('js')

@endsection