@extends ('anasayfa.template')

@section('icerik')

	<section class="p-xlg">
		<div class="container">
			<div class="row">
				<div class="col-md-12 center">
					{!! $anasayfaayar->metin !!}

					<hr class="custom-divider">
				</div>
			</div>

			{{--Menü KAtegori---}}
			<div class="row mt-lg">

					@foreach($restorenmenukategori as $kategori)
					<div class="col-sm-4 pb-xlg">
					<div class="appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">
									<span class="thumb-info thumb-info-no-zoom thumb-info-custom mb-xl center">
										<span class="thumb-info-side-image-wrapper p-none">
											<img width="400px" height="400px" src="/{{$kategori->fotograf}}" class="img-responsive" alt="">
											{{--<img class="thumb-info-custom-icon" src="img/demos/restaurant/icons/restaurant-icon-1.png" alt="" />--}}
										</span>
										<span class="thumb-info-caption">
											<span class="thumb-info-caption-text">
												<h2 class="mb-md mt-xs">{{$kategori->restoranmenu_kategori}}</h2>
												<p class="font-size-md">{{$kategori->restoranmenu_kategori_aciklama}}</p>
												<a class="btn btn-primary mt-md" href="/menuler#{{$kategori->slug}}">Daha Fazla <i class="fa fa-long-arrow-right"></i></a>
											</span>
										</span>
									</span>
					</div>
					</div>
						@endforeach

			</div>
		</div>
	</section>

	{{--//Yorumlar--}}

	<section class="section section-background section-center" style="background-image: url(/anasayfa/img/demos/restaurant/parallax-restaurant.jpg); background-position: 50% 100%; min-height: 400px;">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<hr class="custom-divider">
					<div class="owl-carousel owl-theme nav-bottom rounded-nav" data-plugin-options='{"items": 1, "loop": false}'>
						@foreach($yorumlar as $yorum)
						<div>
							<div class="col-md-12">
								<div class="testimonial testimonial-style-2 testimonial-with-quotes mb-none">
									<blockquote>
										<p>{{$yorum->yorum}}</p>
									</blockquote>
									<div class="testimonial-author">
										<p><strong>{{$yorum->ad}}</strong></p>
									</div>
								</div>
							</div>
						</div>
						@endforeach
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-md-offset-2">

				</div>
			</div>
		</div>
	</section>


	{{--Galeri--}}
	@if($anasayfaayar->galeri == 'evet')
	<section class="p-xlg">
		<div class="container">
			<div class="row mb-xlg">
				<div class="col-md-12 center">
					<h4 class="mt-lg mb-sm">Fotoğraf <strong>Galerisi</strong></h4>
					<p>Mekanımıza ve menülerimize ait fotoğraf galerisi</p>

					<hr class="custom-divider">

					<div class="lightbox" data-plugin-options='{"delegate": "a", "type": "image", "gallery": {"enabled": true}}'>
						<div class="masonry-loader masonry-loader-showing">
							<div class="masonry" data-plugin-masonry data-plugin-options='{"itemSelector": ".masonry-item"}'>
								@foreach($galeri as $galerifoto)
								<div class="masonry-item">
												<span class="thumb-info thumb-info-centered-icons thumb-info-no-borders">
													<span class="thumb-info-wrapper">
														<img width="200" src="/{{$galerifoto->fotograf}}" class="img-responsive" alt="">
														<span class="thumb-info-action thumb-info-action-custom">
															<a href="/{{$galerifoto->fotograf}}">
																<span class="thumb-info-icon-custom"></span>
															</a>
														</span>
													</span>
												</span>
								</div>
								@endforeach

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	@endif
	{{--Galeri--}}

	{{--Blog ve Ekibimiz--}}
	@if($anasayfaayar->blog =='evet')
	<div class="container-fluid">
		<div class="row mt-xlg">
			{{--Blog Yazısı--}}
			<div class="col-md-6 p-none">
				<section class="section section-quaternary section-no-border match-height mt-none" style="min-height: 625px;">
					<div class="row">
						<div class="col-half-section col-half-section-right">
							<div class="center">
								<h4 class="mt-none mb-none heading-dark">Son <strong>Yazılarımız</strong></h4>
								<p class="mb-xs">Lezzetli Yazı Kuşağımız...</p>

								<hr class="custom-divider m-none">
							</div>

							<div class="owl-carousel owl-theme show-nav-title mt-xlg mb-none" data-plugin-options='{"items": 1, "margin": 10, "loop": true, "nav": true, "dots": false, "autoplay": true, "autoplayTimeout": 4000}'>
								@foreach ($blogs->chunk(2) as $array)
								<div>
									@foreach($array as $blog)
										<span class="thumb-info thumb-info-side-image thumb-info-no-zoom thumb-info-no-borders thumb-info-blog-custom mb-xl">
													<span class="thumb-info-side-image-wrapper p-none">
														<img src="{{$blog->resim}}" class="img-responsive" alt="" style="width: 165px;">
													</span>
													<span class="thumb-info-caption">
														<span class="thumb-info-caption-text">
															<h4 class="mb-none mt-xs heading-dark">{{$blog->baslik}}</h4>
															<p class="font-size-md">{{str_limit(strip_tags($blog->icerik),$limit=50,$end='...')}}</p>
															<a class="mt-md" href="demo-restaurant-press-detail.html">Devamını Oku <i class="fa fa-long-arrow-right"></i></a>
														</span>
													</span>
												</span>
									@endforeach
								</div>
								@endforeach




							</div>

						</div>
					</div>
				</section>
			</div>

			{{--////Ekibimiz--}}
			<div class="col-md-6 p-none">
				<section class="section section-tertiary section-no-border match-height mt-none">
					<div class="row">
						<div class="col-half-section">
							<div class="center">
								<h4 class="mt-none mb-none heading-dark">Our <strong>Ekibimiz</strong></h4>
								<p class="mb-xs">Usta eller ve personellerimiz ile hizmetinizdeyiz...</p>

								<hr class="custom-divider m-none">
							</div>

							<div class="owl-carousel owl-theme show-nav-title mt-xlg mb-none" data-plugin-options='{"responsive": {"0": {"items": 1}, "479": {"items": 1}, "768": {"items": 2}, "979": {"items": 2}, "1199": {"items": 2}}, "margin": 10, "loop": false, "nav": true, "dots": false}'>

								@foreach($ekip as $kisi)
								<div>
												<span class="thumb-info thumb-info-no-zoom thumb-info-no-borders mb-xl">
													<span class="thumb-info-side-image-wrapper p-none">
														<img src="/{{$kisi->resim}}" class="img-responsive" alt="">
													</span>
													<span class="thumb-info-caption">
														<span class="thumb-info-caption-text thumb-info-caption-text-custom center">
															<h4 class="mb-none mt-xs heading-dark">{{$kisi->ad}}</h4>
															<p class="font-size-md p-none m-none mb-sm">{{$kisi->gorevi}}</p>
														</span>
													</span>
												</span>
								</div>
								@endforeach

							</div>
						</div>
					</div>
				</section>
			</div>

		</div>
	</div>
	@endif


	{{--Menü--}}
	@if($anasayfaayar->menu =='evet')
	<section id="menu" style="background-image: url(/anasayfa/img/demos/restaurant/background-restaurant.png); background-position: 50% 100%; background-repeat: no-repeat;">
		<div class="container">
			<div class="row mt-xlg">
				<div class="col-md-12 center">
					<h4 class="mt-lg mb-sm">Son Eklenen <strong>Menüler</strong></h4>
					<p>Son Eklenen Menüler</p>

					<hr class="custom-divider">

					<ul class="special-menu pb-xlg">

						@foreach ($restoranmenu as $menu)
						<li>
							<img width="150" src="/{{$menu->resim}}" class="img-responsive" alt="">
							<h3>{{$menu->menuadi}}</h3>
							<p><span>{{strip_tags($menu->menuaciklama)}}</span></p>
							<strong class="special-menu-price text-color-dark">{{$menu->fiyat}} {{$ayar->parabirimi}}</strong>
						</li>
							@endforeach

					</ul>

				</div>
			</div>
			<div class="row mb-xlg mt-xl">
				<div class="col-md-12 center">
					<a href="demo-restaurant-menu.html" class="btn btn-primary btn-lg mt-xlg mb-xlg">Tüm Menüler</a>
				</div>
			</div>
		</div>
	</section>
	@endif

@endsection

@section('css')

@endsection

@section('js')

@endsection