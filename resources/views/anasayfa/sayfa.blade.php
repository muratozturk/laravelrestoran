@extends ('anasayfa.template')

@section('icerik')

{{--
                {{$sayfa->sayfa_basligi}}
                {!! $sayfa->sayfa_icerik !!}
                {{$sayfa->sayfa_one_cikan_foto}}--}}



                <section class="parallax section section-text-light section-parallax section-center mt-none mb-xl" data-plugin-parallax data-plugin-options='{"speed": 1.5}' data-image-src="/{{$sayfa->sayfa_one_cikan_foto}}">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 center">
                                <h1 class="mt-xlg pb-md pt-md font-color-light"><strong>{{$sayfa->sayfa_basligi}}</strong></h1>
                            </div>
                        </div>
                    </div>
                </section>

                <div class="container">
                    <div class="row">
                        <div class="col-md-12 pt-xlg">
                            {!! $sayfa->sayfa_icerik !!}
                        </div>
                    </div>
                </div>


@endsection

@section('css')

@endsection

@section('js')

@endsection