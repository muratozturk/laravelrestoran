@extends('anasayfa.template')

@section('icerik')

    {{--@foreach($haberler as $haber)
        {{$haber->baslik}}
        {{date('d-m-y',strtotime($haber->created_at))}}
        {{$haber->resim}}
        {{str_limit(strip_tags($haber->icerik),$limit=200,$end='...')}}
        /haber/{{$haber->id}}/{{$haber->slug}}
        {{$haberler->links()}}
        @endforeach--}}

    <div class="container">
        <div class="row">
            <div class="col-md-12 center">
                <h1 class="mt-xlg mb-sm pt-md">Lezzetli <strong>&amp; </strong> Yazılarımız</h1>
                <p class="font-size-md">Lezzetli ipuçları ve detaylar için yazılarımızı okuyabilirsiniz...</p>

                <hr class="custom-divider">
            </div>
        </div>
    </div>

    <section class="section section-default mb-none">
        <div class="container pb-xlg">
            @foreach($haberler as $haber)
            <div class="row">
                <div class="col-md-12">
								<span class="thumb-info thumb-info-side-image thumb-info-no-zoom thumb-info-no-borders thumb-info-blog-custom mt-xl">
									<span class="thumb-info-side-image-wrapper p-none hidden-xs">
										<a title="" href="/yazi/{{$haber->id}}/{{$haber->slug}}">
											<img src="/{{$haber->resim}}" class="img-responsive mr-md" alt="" style="width: 235px;">
										</a>
									</span>
									<span class="thumb-info-caption">
										<span class="thumb-info-caption-text">
											<h2 class="mb-md mt-xs"><a title="" class="text-dark" href="/yazi/{{$haber->id}}/{{$haber->slug}}">{{$haber->baslik}}</a></h2>
											<span class="post-meta">
												<span>{{date('d-m-y',strtotime($haber->created_at))}}</span>
											</span>
											<p class="font-size-md">{{str_limit(strip_tags($haber->icerik),$limit=200,$end='...')}}</p>
											<a class="mt-md" href="/yazi/{{$haber->id}}/{{$haber->slug}}">Daha Fazla Oku <i class="fa fa-long-arrow-right"></i></a>
										</span>
									</span>
								</span>

                </div>
            </div>
            @endforeach
           {{-- <div class="row mt-xl">
                <div class="col-md-12">

								<span class="thumb-info thumb-info-side-image thumb-info-no-zoom thumb-info-no-borders thumb-info-blog-custom mt-xl">
									<span class="thumb-info-side-image-wrapper p-none hidden-xs">
										<a title="" href="demo-restaurant-press-detail.html">
											<img src="img/demos/restaurant/blog/blog-restaurant-5.jpg" class="img-responsive mr-md" alt="" style="width: 235px;">
										</a>
									</span>
									<span class="thumb-info-caption">
										<span class="thumb-info-caption-text">
											<h2 class="mb-md mt-xs"><a title="" class="text-dark" href="demo-restaurant-press-detail.html">Lorem ipsum dolor sit</a></h2>
											<span class="post-meta">
												<span>January 10, 2016 | <a href="#">John Doe</a></span>
											</span>
											<p class="font-size-md">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem donec eu pulvinar magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>
											<a class="mt-md" href="demo-restaurant-press-detail.html">Read More <i class="fa fa-long-arrow-right"></i></a>
										</span>
									</span>
								</span>

                </div>
            </div>
            <div class="row pb-xlg">
                <div class="col-md-12">

								<span class="thumb-info thumb-info-side-image thumb-info-no-zoom thumb-info-no-borders thumb-info-blog-custom mt-xl">
									<span class="thumb-info-side-image-wrapper p-none hidden-xs">
										<a title="" href="demo-restaurant-press-detail.html">
											<img src="img/demos/restaurant/blog/blog-restaurant-6.jpg" class="img-responsive mr-md" alt="" style="width: 235px;">
										</a>
									</span>
									<span class="thumb-info-caption">
										<span class="thumb-info-caption-text">
											<h2 class="mb-md mt-xs"><a title="" class="text-dark" href="demo-restaurant-press-detail.html">Lorem ipsum dolor sit</a></h2>
											<span class="post-meta">
												<span>January 10, 2016 | <a href="#">John Doe</a></span>
											</span>
											<p class="font-size-md">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem donec eu pulvinar magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>
											<a class="mt-md" href="demo-restaurant-press-detail.html">Read More <i class="fa fa-long-arrow-right"></i></a>
										</span>
									</span>
								</span>

                </div>
            </div>--}}

                {{$haberler->links()}}
        </div>
    </section>




@endsection

@section('css')

@endsection

@section('js')

@endsection