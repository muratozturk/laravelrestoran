<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{$ayar->site_adi}}</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="keywords" content="" />
    <meta name="description" content="">
    <meta name="author" content="tagajans.com">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="/{{$ayar->favicon}}"/>
    <link rel="apple-touch-icon" href="/{{$ayar->favicon}}">
    <!--===============================================================================================-->
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="/anasayfa/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/anasayfa/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/anasayfa/vendor/animate/animate.min.css">
    <link rel="stylesheet" href="/anasayfa/vendor/simple-line-icons/css/simple-line-icons.min.css">
    <link rel="stylesheet" href="/anasayfa/vendor/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="/anasayfa/vendor/owl.carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="/anasayfa/vendor/magnific-popup/magnific-popup.min.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="/anasayfa/css/theme.css">
    <link rel="stylesheet" href="/anasayfa/css/theme-elements.css">
    <link rel="stylesheet" href="/anasayfa/css/theme-blog.css">
    <link rel="stylesheet" href="/anasayfa/css/theme-shop.css">

    <!-- Current Page CSS -->
    <link rel="stylesheet" href="/anasayfa/vendor/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="/anasayfa/vendor/rs-plugin/css/layers.css">
    <link rel="stylesheet" href="/anasayfa/vendor/rs-plugin/css/navigation.css">

    <!-- Skin CSS -->
    <link rel="stylesheet" href="/anasayfa/css/skins/skin-restaurant.css">

    <!-- Demo CSS -->
    <link rel="stylesheet" href="/anasayfa/css/demos/demo-restaurant.css">

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="/anasayfa/css/custom.css">
    <script src="/anasayfa/vendor/modernizr/modernizr.min.js"></script>
    <!--===============================================================================================-->
    @yield('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
    <!-- Head Libs -->

</head>
<body data-spy="scroll" data-target="#navSecondary" data-offset="170">
<div class="body">
    @include('anasayfa.include.header')

    <div role="main" class="main">
        <!-- Slider -->
        @if(request()->route()->getName()=='anasayfa')
            @include('anasayfa.include.slider')
        @endif
    <!-- Slider -->

        @yield('icerik')

    </div>

    @include ('anasayfa.include.footer')

</div>
<!--===============================================================================================-->
<!-- Vendor -->
<script src="/anasayfa/vendor/jquery/jquery.min.js"></script>
<script src="/anasayfa/vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="/anasayfa/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="/anasayfa/vendor/jquery-cookie/jquery-cookie.min.js"></script>
<script src="/anasayfa/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="/anasayfa/vendor/common/common.min.js"></script>
<script src="/anasayfa/vendor/jquery.validation/jquery.validation.min.js"></script>
<script src="/anasayfa/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<script src="/anasayfa/vendor/jquery.gmap/jquery.gmap.min.js"></script>
<script src="/anasayfa/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="/anasayfa/vendor/isotope/jquery.isotope.min.js"></script>
<script src="/anasayfa/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="/anasayfa/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="/anasayfa/vendor/vide/vide.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="/anasayfa/js/theme.js"></script>

<!-- Current Page Vendor and Views -->
<script src="/anasayfa/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="/anasayfa/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- Theme Custom -->
<script src="/anasayfa/js/custom.js"></script>

<!-- Theme Initialization Files -->
<script src="/anasayfa/js/theme.init.js"></script>

@yield('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>
@include('sweetalert::alert')
</body>
</html>