@extends('anasayfa.template')

@section('icerik')

    <div class="container">
        <div class="row">
            <div class="col-md-12 center">
                <h1 class="mt-xlg mb-sm pt-md">Fotoğraf <strong>&amp; </strong> Galerisi</h1>
                <p class="font-size-md">Mekan ve Konsept Fotoğraflarımız...</p>

                <hr class="custom-divider">
            </div>
        </div>
    </div>

    <section class="p-xlg">
        <div class="container">
            <div class="row mb-xlg">
                <div class="col-md-12 center">
                    <div class="lightbox" data-plugin-options='{"delegate": "a", "type": "image", "gallery": {"enabled": true}}'>
                        <div class="masonry-loader masonry-loader-showing">
                            <div class="masonry" data-plugin-masonry data-plugin-options='{"itemSelector": ".masonry-item"}'>
                                @foreach($fotolar as $galerifoto)
                                    <div class="masonry-item">
												<span class="thumb-info thumb-info-centered-icons thumb-info-no-borders">
													<span class="thumb-info-wrapper">
														<img width="200" src="/{{$galerifoto->fotograf}}" class="img-responsive" alt="">
														<span class="thumb-info-action thumb-info-action-custom">
															<a href="/{{$galerifoto->fotograf}}">
																<span class="thumb-info-icon-custom"></span>
															</a>
														</span>
													</span>
												</span>
                                    </div>
                                @endforeach

                            </div>
                            {{$fotolar->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

@section('css')

@endsection

@section('js')

@endsection