@extends ('anasayfa.template')

@section('icerik')

    <div class="container">
        <div class="row">
            <div class="col-md-12 center">
                <h1 class="mt-xlg mb-sm pt-md">Bize <strong>Ulaşın</strong></h1>
                <p class="font-size-md">Tüm soru ve görüşleriniz için bize ulaşın...</p>

                <hr class="custom-divider">
            </div>
        </div>
    </div>

    <section class="section section-default mb-none">
        <div class="container">
            <div class="row">
                <div class="col-md-4">

                    <h5 class="mb-xs mt-xl">Rezervasyon</h5>
                    <p><i class="fa fa-phone"></i>{{$ayar->telefon}}</p>

                    <h5 class="mb-xs mt-xl">Telefon</h5>
                    <p><i class="fa fa-phone"></i>{{$ayar->telefon}}</p>

                    <h5 class="mb-xs mt-xl">Adres</h5>
                    <p><i class="fa fa-map-marker"></i>{{$ayar->firma_adres}}</p>

                    <div id="googlemaps" class="google-map small">{!! $ayar->googlemap !!}</div>



                </div>
                <div class="col-md-4">
                    <h5 class="mb-xs mt-xl">Öğle Yemeği</h5>

                    <ul class="list list-icons list-dark mt-md">
                        <li><i class="fa fa-clock-o"></i> Pazartesi Cuma - 9am to 5pm</li>
                        <li><i class="fa fa-clock-o"></i> Cumartesi - 9am to 2pm</li>
                        <li><i class="fa fa-clock-o"></i> Pazar - Closed</li>
                    </ul>

                    <h5 class="mb-xs mt-xlg">Akşam Yemeği</h5>

                    <ul class="list list-icons list-dark mt-md">
                        <li><i class="fa fa-clock-o"></i> Monday - Friday - 9am to 5pm</li>
                        <li><i class="fa fa-clock-o"></i> Saturday - 9am to 2pm</li>
                        <li><i class="fa fa-clock-o"></i> Sunday - Closed</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h5 class="mb-md mt-xl">Bize Yazın</h5>

                    <form id="contact-form" class="validate-form" method="post" action="{{route('iletisimformu.gondeer')}}" name="contact">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Adınız</label>
                                    <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="ad" id="name" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>E-Posta</label>
                                    <input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Konu</label>
                                    <input type="text" value="" data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="konu" id="subject" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Mesaj</label>
                                    <textarea maxlength="5000" data-msg-required="Please enter your message." rows="3" class="form-control" name="mesaj" id="message" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" value="Send Message" class="btn btn-primary" data-loading-text="Loading...">

                                <div class="alert alert-success hidden" id="contactSuccess">
                                    Message has been sent to us.
                                </div>

                                <div class="alert alert-danger hidden" id="contactError">
                                    Error sending your message.
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>

@endsection

@section('css')

@endsection

@section('js')

@endsection