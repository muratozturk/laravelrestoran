<header id="header" class="header-narrow" data-plugin-options='{"stickyEnabled": true, "stickyEnableOnBoxed": true, "stickyEnableOnMobile": true, "stickyStartAt": 57, "stickySetTop": "-57px", "stickyChangeLogo": false}'>
    <div class="header-body">
        <div class="header-top header-top-secondary header-top-style-3">
            <div class="container">
                <nav class="header-nav-top pull-right">
                    <ul class="nav nav-pills">
                        <li class="hidden-xs">
                            <span class="ws-nowrap"><i class="fa fa-map-marker"></i> {{$ayar->firma_adres}}</span>
                        </li>
                        <li>
                            <span class="ws-nowrap"><i class="fa fa-phone"></i> {{$ayar->telefon}}</span>
                        </li>
                        <li class="hidden-xs">
                            <span class="ws-nowrap"><i class="fa fa-envelope"></i> <a href="mailto:{{$ayar->email}}">{{$ayar->email}}</a></span>
                        </li>
                    </ul>
                </nav>
                <p class="hidden-xs text-color-tertiary">
                    Lezzet ile
                </p>
            </div>
        </div>
        <div class="header-container container">
            <div class="header-row">
                <div class="header-column">
                    <div class="header-logo">
                        <a href="{{route('anasayfa')}}">
                            <img alt="Porto" width="164" height="55" src="/{{$ayar->logo}}">
                        </a>
                    </div>
                </div>
                <div class="header-column">
                    <div class="header-row">
                        <div class="header-nav">
                            <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
                                <i class="fa fa-bars"></i>
                            </button>
                            <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">
                                <nav>
                                    <ul class="nav nav-pills" id="mainNav">
                                        <li class="activedropdown-full-color dropdown-secondary">
                                            <a href="{{route('anasayfa')}}">
                                                ANASAYFA
                                            </a>
                                        </li>
                                        @foreach($menuler as $menu)
                                            <li class="dropdown-full-color dropdown-secondary">

                                                @if($menu->sayfa_id == 0)
                                                    <a href="{{$menu->ozel_url}}">{{$menu->menu_baslik}}</a>

                                                @else
                                                    <a href="/sayfa/{{$menu->sayfa_id}}/{{$menu->slug}}">{{$menu->menu_baslik}}</a>
                                                @endif
                                                @if($menu->altmenusu->count())
                                                    <ul class="sub-menu">
                                                        @foreach($menu->altmenusu as $altmenu)

                                                            @if ($altmenu->sayfa_id == 0)
                                                                <li><a href="{{$altmenu->ozel_url}}">{{$altmenu->menu_baslik}}</a></li>
                                                            @else
                                                                <li><a href="/sayfa/{{$altmenu->sayfa_id}}/{{$altmenu->slug}}">{{$altmenu->menu_baslik}}</a></li>
                                                            @endif
                                                        @endforeach
                                                    </ul>
                                                @endif

                                            </li>

                                        @endforeach

                                        <li class="dropdown-full-color dropdown-secondary">
                                            <a href="{{route('menu.sayfa')}}">
                                                MENÜ
                                            </a>
                                        </li>

                                        <li class="dropdown-full-color dropdown-secondary">
                                            <a href="{{route('yazilar.goster')}}">
                                                YAZILARIMIZ
                                            </a>
                                        </li>
                                        <li class="dropdown-full-color dropdown-secondary">
                                            <a href="{{route('foto.galeri')}}">
                                                GALERİ
                                            </a>
                                        </li>
                                        <li class="dropdown-full-color dropdown-secondary">
                                            <a href="{{route('iletisim.formu')}}">
                                                İLETİŞİM
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>