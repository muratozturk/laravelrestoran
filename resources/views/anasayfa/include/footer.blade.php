<footer id="footer" class="color color-secondary short">
    <div class="container">
        <div class="row">
            <div class="col-md-12 center">
                <ul class="social-icons mb-md">
                    <li class="social-icons-instagram"><a href="{{$ayar->instagram}}" target="_blank" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                    <li class="social-icons-facebook"><a href="{{$ayar->facebook}}" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li class="social-icons-twitter"><a href="{{$ayar->twitter}}" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                    <li class="social-icons-googleplus"><a href="{{$ayar->pinterest}}" target="_blank" title="pinterest"><i class="fa fa-pinterest"></i></a></li>
                    <li class="social-icons-linkedin"><a href="{{$ayar->youtube}}" target="_blank" title="Linkedin"><i class="fa fa-youtube"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-12 center">
                    <p><i class="fa fa-map-marker"></i> {{$ayar->firma_adres}} <span class="separator">|</span> <i class="fa fa-phone"></i> {{$ayar->telefon}} <span class="separator">|</span> <i class="fa fa-envelope"></i> <a href="mailto:{{$ayar->email}}">{{$ayar->email}}</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>