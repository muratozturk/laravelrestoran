<div class="slider-container rev_slider_wrapper" style="height: 650px;">
    <div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options='{"delay": 9000, "gridwidth": 1170, "gridheight": 650, "disableProgressBar": "on"}'>
        <ul>
            @foreach($sliders as $slider)
            <li data-transition="fade">
                <img src="/{{$slider->slider_resmi}}"
                     alt=""
                     data-bgposition="center center"
                     data-bgfit="cover"
                     data-bgrepeat="no-repeat"
                     class="rev-slidebg">

                {{--<div class="tp-caption top-label alternative-font"
                     data-x="left" data-hoffset="25"
                     data-y="center" data-voffset="-55"
                     data-start="500"
                     style="z-index: 5"
                     data-transform_in="y:[-300%];opacity:0;s:500;">WELCOME TO
                </div>--}}

                <div class="tp-caption"
                     data-x="left" data-hoffset="185"
                     data-y="center" data-voffset="-55"
                     data-start="1000"
                     style="z-index: 5"
                     data-transform_in="x:[300%];opacity:0;s:500;">
                    <img src="img/slides/slide-title-border.png" alt="">
                </div>

                <div class="tp-caption main-label"
                     data-x="left" data-hoffset="25"
                     data-y="center" data-voffset="-5"
                     data-start="1500"
                     data-whitespace="nowrap"
                     data-transform_in="y:[100%];s:500;"
                     data-transform_out="opacity:0;s:500;"
                     style="z-index: 5"
                     data-mask_in="x:0px;y:0px;">{{$slider->metin1}}</div>

                <div class="tp-caption bottom-label"
                     data-x="left" data-hoffset="25"
                     data-y="center" data-voffset="40"
                     data-start="2000"
                     style="z-index: 5"
                     data-transform_in="y:[100%];opacity:0;s:500;" style="font-size: 1.2em;">{{$slider->metin2}}</div>

                <a class="tp-caption btn btn-md btn-primary"
                   data-hash
                   data-hash-offset="85"
                   href="{{$slider->buton_linki}}"
                   data-x="left" data-hoffset="25"
                   data-y="center" data-voffset="85"
                   data-start="2200"
                   data-whitespace="nowrap"
                   data-transform_in="y:[100%];s:500;"
                   data-transform_out="opacity:0;s:500;"
                   style="z-index: 5"
                   data-mask_in="x:0px;y:0px;">{{$slider->buton_metni}} <i class="fa fa-long-arrow-down"></i></a>

            </li>
            @endforeach
        </ul>
    </div>
</div>