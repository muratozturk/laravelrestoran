<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});
Route::group(['prefix'=>'yonetim','middleware' =>'admin'],function(){
Route::get('/','YonetimController@index')->name('yonetim.index');
Route::get('cikis','YonetimController@cikis')->name('yonetim.cikis');


Route::resource('ayarlar','AyarController');
Route::resource('bloglar','BlogController');
Route::resource('kategori','KategoriController');
Route::resource('menuler','MenuController');
Route::resource('sayfalar','SayfaController');
Route::resource('hizmetler','HizmetController');
Route::resource('sliderlar','SliderController');
Route::resource('referanslar','ReferansController');
Route::resource('yazilar','YaziController');
Route::resource('anasayfaayar','AnasayfaAyarController');
Route::resource('ekip','EkipController');
Route::resource('restoranmenukategori','RestoranMenuKategoriController');
Route::resource('restoranmenu','RestoranMenuController');
Route::resource('yorumlar','YorumlarController');
Route::resource('galeri','GaleriController');
Route::resource('anasayfaayar','AnasayfaAyarController');

Route::get('kullanicilar','YonetimController@kullanicilar')->name('kullanicilar.index');
Route::get('kullaniciekle','YonetimController@kullaniciekle')->name('kullanici.ekle');
Route::post('kullanicikayit','YonetimController@kullanicikayit')->name('kullanici.kayit');
Route::get('kullaniciduzenle/{id}','YonetimController@kullaniciduzenle')->name('kullanici.duzenle');
Route::post('kullaniciguncelle/{id}','YonetimController@kullaniciguncelle')->name('kullanici.guncelle');
Route::delete('kullanicisil/{id}','YonetimController@kullanicisil')->name('kullanici.sil');
Route::get('iletisim','YonetimController@iletisim')->name('iletisim');
Route::post('iletisim','YonetimController@iletisimgonder')->name('iletisim.gonder');

//Galeri

Route::get('galeri', 'GaleriController@index')->name('galeri');
Route::post('galeri/uploadFiles', 'GaleriController@multiple_upload');
Route::delete('galeridelete/{id}', 'GaleriController@destroy')->name('foto.sil');
Route::post('galeriupdate/{id}', 'GaleriController@update')->name('foto.update');

});

Auth::routes(['register' => false]);

//Anasayfa
Route::get('/','HomeController@index')->name('anasayfa');

//Sayfalar
Route::get('sayfa/{id}/{slug}', 'HomeController@sayfa')->name('sayfa.goster');
Route::get('/iletisim', 'HomeController@iletisim')->name('iletisim.formu');
Route::get('/menuler', 'HomeController@restoranmenu')->name('menu.sayfa');
Route::get('/fotogaleri', 'HomeController@fotogaleri')->name('foto.galeri');
Route::post('iletisimformu','HomeController@iletisimformgonder')->name('iletisimformu.gondeer');
//anasayfa iletşim formu için
Route::get('anasayfailetisim','HomeController@anasayfailetisim')->name('anasayfailetisim.formu');
Route::post('anasayfaform','HomeController@anasayfaformgonder')->name('anasayfaform.gonder');




//Hizmetler
Route::get('/hizmetler','HomeController@hizmetler')->name('hizmetleri.goster');
Route::get('/hizmet/{id}/{slug}','HomeController@hizmet');

//Haberler (İcerikler)
Route::get('/yazilar','HomeController@haberler')->name('yazilar.goster');
Route::get('/yazi/{id}/{slug}','HomeController@haber');

