<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRestoranmenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restoranmenu', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kategori');
            $table->string('menuadi');
            $table->string('menuaciklama')->nullable();
            $table->decimal('fiyat',8,2)->nullable();//8tam sayı ve 2 ondalık
            $table->string('resim');
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restoranmenu');
    }
}
