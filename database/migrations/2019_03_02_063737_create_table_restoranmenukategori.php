<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRestoranmenukategori extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restoranmenu_kategori', function (Blueprint $table) {
            $table->increments('id');
            $table->string('restoranmenu_kategori');
            $table->string('restoranmenu_kategori_aciklama')->nullable();
            $table->string('fotograf');
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restoranmenu_kategori');
    }
}
