<?php

namespace App\Http\Controllers;
use App\AnasayfaAyar;
use App\Ayar;
use App\Ekip;
use App\Galeri;
use App\Hizmet;
use App\Kategori;
use App\Referans;
use App\RestoranMenu;
use App\RestoranmenuKategori;
use App\Sayfa;
use App\Menu;
use App\Slider;
use App\Yazi;
use App\Yorumlar;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{

   /* public function __construct()
    {
        $this->middleware('auth');
    }*/


    public function index()
    {
        $sliders = Slider::all();
        $anasayfaayar = AnasayfaAyar::find(1);
        $restorenmenukategori = RestoranmenuKategori::orderby('created_at','desc')->take(3)->get();
        $restoranmenu = RestoranMenu::orderby('created_at','desc')->take(5)->get();
        $blogs = Yazi::orderby('created_at','desc')->take(6)->get();
        $galeri = Galeri::orderby('created_at','desc')->take(8)->get();
        $ekip = Ekip::all();
        $yorumlar = Yorumlar::orderby('created_at','desc')->take(5)->get();
        return view('anasayfa.index',compact(
            'sliders','restorenmenukategori','blogs','partners','anasayfaayar','ekip','yorumlar','galeri','restoranmenu'
        ));
    }



    public function cikis()  {

        auth()->logout();
        return redirect('/');
    }


    public function hakkimizda(){
        return view('anasayfa.hakkimizda');
    }

    public function sayfa($id){

        $sayfa = Sayfa::find($id);
        return view('anasayfa.sayfa',compact('sayfa'));


    }

    public function restoranmenu(){


        $restoranmenuleri = RestoranMenu::all();
        $restoranmenukategoriler = RestoranmenuKategori::all();

        return view('anasayfa.restoranmenu',compact('restoranmenuleri','restoranmenukategoriler'));

    }

    public function hizmetler(){

        $hizmetler = Hizmet::all();
        return view('anasayfa.hizmetler', compact('hizmetler'));

    }

    public function hizmet($id){

        $hizmet = Hizmet::find($id);
        $listhizmetler = Hizmet::all();
            return view('anasayfa.hizmet', compact('hizmet','listhizmetler'));
        }

    public function haberler(){
            $haberler = Yazi::orderby('created_at','desc')->paginate(4);
            return view('anasayfa.yazilar',compact('haberler'));
    }

    public function haber($id){

        $haber = Yazi::find($id);

        return view('anasayfa.yazi',compact('haber'));

    }
    public function iletisim(){
        return view('anasayfa.iletisim');
    }

    public function iletisimformgonder(Request $request){

        $this->validate($request,array(
            'ad'=>'required',
            'email'=>'required',
            'konu'=>'required',
            'mesaj'=>'required',
        ));

        $ayarlar = Ayar::find(1);
        $siteadi = $ayarlar->site_adi;
        $siteemail = $ayarlar->email;

        $bilgiler = array(
            'ad' => request('ad'),
            'email' => request('email'),
            'konu' => request('konu'),
            'mesaj' => request('mesaj'),
            'siteadi' => $siteadi,
            'siteemail' => $siteemail,

        );
        Mail::to($siteemail)->send(new \App\Mail\iletisimformu($bilgiler));
           alert()
             ->success('Gönderildi','Formunuz Gönderildi')
             ->autoClose(2000);
           return back();


    }

    public function anasayfaformgonder(Request $request){

        $this->validate($request,array(
            'ad'=>'required',
            'email'=>'required',
            'konu'=>'required',

        ));
        $ayarlar = Ayar::find(1);
        $siteadi = $ayarlar->site_adi;
        $siteemail = $ayarlar->email;

        $bilgiler = array(
            'ad' => request('ad'),
            'email' => request('email'),
            'konu' => request('konu'),
            'siteadi' => $siteadi,
            'siteemail' => $siteemail,

        );
        Mail::to($siteemail)->send(new \App\Mail\talepformu($bilgiler));
        alert()
            ->success('Gönderildi','Formunuz Gönderildi')
            ->autoClose(1000);
        return back();


    }

    public function fotogaleri(){

        $fotolar = Galeri::orderby('created_at','desc')->paginate(20);
        return view('anasayfa.galeri',compact('fotolar'));

    }

}
