<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Sayfa;
use Illuminate\Http\Request;

class MenuController extends Controller
{

    public function index()
    {
        $menuler = Menu::all();
        return view('admin.menuler.index',compact('menuler'));
    }


    public function create()
    {
        $sayfalar = Sayfa::all();
        $menuler = Menu::where('menu_ust_id','0')->get();
        return view('admin.menuler.create',compact('sayfalar','menuler'));

    }

    public function store(Request $request)
    {
        $this->validate(request(),array(

            'menu_baslik'=>'required',
        ));


        $menu = new Menu();
        $menu->menu_baslik = request('menu_baslik');
        $menu->ozel_url = request('ozel_url');
        $menu->menu_ust_id = request('menu_ust_id');
        $menu->sayfa_id = request('sayfa_id');
        $menu->sira_no = request('sira_no');
        $menu->slug = str_slug (request('menu_baslik'));

        $menu->save();
        if($menu){
            alert()
                ->success('Başarılı','İşlem Başarılı')
                ->autoClose(1000);
            return back();


        }else {
            alert()
                ->error('Hata','İşlem Başarısız')
                ->autoClose(1000);
            return back();

        }

    }

    public function show($id)
    {

    }


    public function edit($id)
    {
        $menu = Menu::find($id);
        $sayfalar = Sayfa::where('id','!=', $menu->sayfa_basligi)->get();
        $tummenuler = Menu::all();
        return view('admin.menuler.edit', compact('menu','sayfalar','tummenuler'));


    }


    public function update(Request $request, $id)
    {
        $this->validate(request(),array(

            'menu_baslik'=>'required',



        ));


        $menu = Menu::find($id);
        $menu->menu_baslik = request('menu_baslik');
        $menu->ozel_url = request('ozel_url');
        $menu->menu_ust_id = request('menu_ust_id');
        $menu->sayfa_id = request('sayfa_id');
        $menu->sira_no = request('sira_no');
        $menu->slug = str_slug(request('menu_baslik'));

        $menu->save();
        if($menu){
            alert()
                ->success('Başarılı','İşlem Başarılı')
                ->showConfirmButton()

                ->autoClose(2000);
            return back();


        }else {
            alert()
                ->error('Hata','İşlem Başarısız')
                ->autoClose(1000);
            return back();

        }




    }


    public function destroy($id)
    {
        $sil = Menu::destroy($id);
        if ($sil) {
            alert()
                ->success('Başarılı','İşlem Başarılı')
                ->showConfirmButton()

                ->autoClose(2000);
            return back();


        }else {
            alert()
                ->error('Hata','İşlem Başarısız')
                ->autoClose(1000);
            return back();


        }

    }

}
