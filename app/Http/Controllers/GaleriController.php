<?php

namespace App\Http\Controllers;

use App\Galeri;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class GaleriController extends Controller
{

    public function index()
    {
        $fotograflar = Galeri::all();
        return view('admin.galeri.index', compact('fotograflar'));
    }
    public function multiple_upload() {
        // getting all of the post data
        $files = Input::file('images');
        // Making counting of uploaded images
        $file_count = count($files);
        // start count how many uploaded
        $uploadcount = 0;

        foreach ($files as $file) {
            $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
            $validator = Validator::make(array('file'=> $file), $rules);
            if($validator->passes()){

                $galeri = new Galeri();
                $dosya_adi = 'galeri' . '-' . microtime(true)  . '.' . $file->extension();
                $hedef_klasor = 'uploads/dosyalar/galeri';
                $dosya_yolu = $hedef_klasor . '/' . $dosya_adi;
                $file->move($hedef_klasor, $dosya_adi);
                $galeri->fotograf = $dosya_yolu;
                $galeri->seo = str_slug($file->getClientOriginalName());
                $galeri->original_filename = $file->getClientOriginalName();

                $galeri->save();
            }
        }
        if($galeri){
            alert()
                ->success('Başarılı', 'Fotoğraflar Yüklendi')
                ->autoClose(1000);
            return back();
        } else {
            alert()
                ->error('Hata', 'Yüklenemedi')
                ->autoClose(1000);
            return back();
        }
    }

    public function update(Request $request, $id){


    }

    public function destroy($id)
    {
        $fotograf = Galeri::find($id);
        $sildosya = $fotograf->filename;
        File::delete($sildosya);
        $sil = Galeri::destroy($id);
        if ($sil) {

            alert()
                ->success('Başarılı', 'Silindi')
                ->autoClose(1000);
            return back();


        } else {
            alert()
                ->error('Hata', 'İşlem Başarısız')
                ->autoClose(1000);
            return back();

        }
    }
}
