<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Sayfa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class SayfaController extends Controller
{

    public function index()
    {
        $sayfalar = Sayfa::all();
        return view('admin.sayfalar.index',compact('sayfalar'));
    }

    public function create()
    {
        return view('admin.sayfalar.create');
    }


    public function store(Request $request)
    {
        $this->validate(request(),array(
            'sayfa_basligi'=>'required',
            'sayfa_icerik'=>'required',

        ));

        $sayfa  = new Sayfa();
        $sayfa->sayfa_basligi = request('sayfa_basligi');
        $sayfa->sayfa_icerik = request('sayfa_icerik');
        $sayfa->slug = str_slug (request('sayfa_basligi'));
        $sayfa->save();

        if (request()->hasFile('sayfa_one_cikan_foto')) {

            $validator = Validator::make($request->all(), [
                'sayfa_one_cikan_foto' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            ]);
            if (!$validator->passes()) {
                alert()
                    ->error('Foto Yüklenemedi', 'Foto Dosya Boyutu Çok Büyük')

                    ->autoClose(1000);
                return back();
            }

                $resim = request()->file('sayfa_one_cikan_foto');
                $dosya_adi = 'sayfa_one_cikan_foto' . '-' . time() . '.' . $resim->extension();

                if ($resim->isValid()) {

                    $hedef_klasor = 'uploads/dosyalar/klas_sayfa';
                    $dosya_yolu = $hedef_klasor . '/' . $dosya_adi;
                    $resim->move($hedef_klasor, $dosya_adi);
                    $sayfa->sayfa_one_cikan_foto = $dosya_yolu;


                }
            }
        $sayfa->save();

        if($sayfa){
            alert()
                ->success('Başarılı','İşlem Başarılı')


                ->autoClose(1000);
            return back();


        }else {
            alert()
                ->error('Hata','İşlem Başarısız')


                ->autoClose(1000);
            return back();

        }
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $sayfa = Sayfa::find($id);
        return view('admin.sayfalar.edit',compact('sayfa'));
    }

    public function update(Request $request, $id)
    {
        $this->validate(request(),array(
            'sayfa_basligi'=>'required',
            'sayfa_icerik'=>'required',

        ));
        $sayfa = Sayfa::find($id);
        $sayfa->sayfa_basligi = request('sayfa_basligi');
        $sayfa->sayfa_icerik = request('sayfa_icerik');
        $sayfa->slug = str_slug (request('sayfa_basligi'));
        if (request()->hasFile('sayfa_one_cikan_foto')) {

            $validator = Validator::make($request->all(), [
                'sayfa_one_cikan_foto' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            ]);
            if (!$validator->passes()) {
                alert()
                    ->error('Foto Yüklenemedi', 'Foto Dosya Boyutu Çok Büyük')

                    ->autoClose(1000);
                return back();
            }

            $resim = request()->file('sayfa_one_cikan_foto');
            $dosya_adi = 'sayfa_one_cikan_foto' . '-' . time() . '.' . $resim->extension();

            if ($resim->isValid()) {

                $hedef_klasor = 'uploads/dosyalar/klas_sayfa';
                $dosya_yolu = $hedef_klasor . '/' . $dosya_adi;
                $resim->move($hedef_klasor, $dosya_adi);
                $sayfa->sayfa_one_cikan_foto = $dosya_yolu;


            }
        }
        $sayfa->save();


             if($sayfa){
                 alert()
                     ->success('Başarılı','İşlem Başarılı')
                     ->showConfirmButton()
                     ->autoClose(1000);
                 return back();


             }else {
                 alert()
                     ->error('Hata','İşlem Başarısız')
                     ->showConfirmButton()

                     ->autoClose(1000);
                 return back();

             }
    }


    public function destroy($id)
    {

        // menu modeli çağrılıp , menu içerisinde sayfa_id de gelen $id var ise o satır silinir
        $menu = Menu::where('sayfa_id',$id)->count();
        if($menu>0){

            DB::table('menuler')->where('sayfa_id', '=', $id)->delete();

            alert()
                ->success('Başarılı', 'Menu Silindi')

                ->autoClose(1000);

        }
        $fotograf = Sayfa::find($id);
        $sildosya = $fotograf->sayfa_one_cikan_foto;
        File::delete($sildosya);

            $sil = Sayfa::destroy($id);
            if ($sil) {
                alert()
                    ->success('Başarılı', 'İşlem Başarılı')

                    ->autoClose(1000);
                return back();


            } else {
                alert()
                    ->error('Hata', 'İşlem Başarısız')

                    ->autoClose(1000);
                return back();

            }

    }


}
