<?php

namespace App\Http\Controllers;

use App\Yorumlar;
use Illuminate\Http\Request;

class YorumlarController extends Controller
{
    public function index()
    {
        $yorumlar = Yorumlar::all();
        return view('admin.yorumlar.index',compact('yorumlar'));
    }

    public function create()
    {
        return view ('admin.yorumlar.create');
    }

    public function store(Request $request)
    {
        $yorum = new Yorumlar();
        $yorum->ad = request('ad');
        $yorum->yorum = request('yorum');
        $yorum->email = request('email');
        $yorum->slug = str_slug(request('ad'));

        $yorum->save();
        if($yorum){

            alert()
                ->success('Başarılı', 'Kategori Eklendi')
                ->autoClose(2000);
            return back();
        }else{
            alert()
                ->error('Başarısız', 'Kategori Eklenemedi')
                ->autoClose(2000);
            return back();
        }
    }

    public function edit($id)
    {
        $yorum = Yorumlar::find($id);
        return view('admin.yorumlar.edit', compact('yorum'));
    }


    public function update(Request $request, $id)
    {
        $yorum = Yorumlar::find($id);
        $yorum->ad = request('ad');
        $yorum->yorum = request('yorum');
        $yorum->email = request('email');
        $yorum->slug = str_slug(request('ad'));

        $yorum->save();
        if($yorum){

            alert()
                ->success('Başarılı', 'Yorum Güncellendi')
                ->autoClose(2000);
            return back();
        }else{
            alert()
                ->error('Başarısız', 'Güncellenemedi')
                ->autoClose(2000);
            return back();
        }
    }


    public function destroy($id)
    {

        $yorum = Yorumlar::destroy($id);

        if($yorum){
            alert()
                ->success('Silindi', 'Yorum Silindi')
                ->autoClose(1000);
            return back();
        }else{
            alert()
                ->error('Başarısız', 'Silinemedi')
                ->autoClose(2000);
            return back();
        }
    }
}
