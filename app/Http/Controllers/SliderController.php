<?php

namespace App\Http\Controllers;
use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class SliderController extends Controller
{

    public function index()
    {
        $sliderlar = Slider::all();
        return view('admin.sliderlar.index', compact('sliderlar'));
    }


    public function create()
    {
        return view('admin.sliderlar.create');
    }


    public function store(Request $request)
    {
        $this->validate(request(), array(
            'metin1' => 'required',
            'metin2' => 'required',

        ));

        $slider = new Slider();
        $slidersonsira = Slider::max('sira_no');
        $slidergelensira = request('sira_no');
        if($slidergelensira == 0){
            $slider->sira_no = $slidersonsira + 1 ;
        }else{
            $slider->sira_no = request('sira_no');
        }


        $slider->metin1 = request('metin1');
        $slider->metin2 = request('metin2');
        $slider->buton_metni = request('buton_metni');
        $slider->buton_linki = request('buton_linki');

        $slider = Slider::create($request->all());
        if (request()->hasFile('slider_resmi')) {

            $validator = Validator::make($request->all(), [
                'slider_resmi' => 'image|mimes:jpeg,png,jpg,gif,svg|max:512',
            ]);
            if (!$validator->passes()) {
                alert()
                    ->error('Foto Yüklenemedi', 'Foto Dosya Boyutu Çok Büyük')

                    ->autoClose(1000);
                return back();
            }


            $resim = request()->file('slider_resmi');
            $dosya_adi = 'slider_resmi' . '-' . time() . '.' . $resim->extension();

                if ($resim->isValid()) {

                    $hedef_klasor = 'uploads/dosyalar/klas_slid';
                    $dosya_yolu = $hedef_klasor . '/' . $dosya_adi;
                    $resim->move($hedef_klasor, $dosya_adi);
                    $slider->slider_resmi = $dosya_yolu;


                }
            }
        $slider->save();


        if ($slider) {
            alert()
                ->success('Başarılı', 'İşlem Başarılı')

                ->autoClose(1000);
            return back();


        } else {
            alert()
                ->error('Hata', 'İşlem Başarısız')

                ->autoClose(1000);
            return back();

        }
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $slider = Slider::find($id);
        return view('admin.sliderlar.edit', compact('slider'));
    }


    public function update(Request $request, $id)
    {
        $this->validate(request(), array(
            'metin1' => 'required',
            'metin2' => 'required',

        ));
        $slider = Slider::find($id);
        $bilgiler = $request->all();
        $slider->update($bilgiler);
        if (request()->hasFile('slider_resmi')) {

            $validator = Validator::make($request->all(), [
                'slider_resmi' => 'image|mimes:jpeg,png,jpg,gif,svg|max:512',
            ]);
            if (!$validator->passes()) {
                alert()
                    ->error('Foto Yüklenemedi', 'Foto Dosya Boyutu Çok Büyük')

                    ->autoClose(1000);
                return back();
            }


            $resim = request()->file('slider_resmi');
            $dosya_adi = 'slider_resmi' . '-' . time() . '.' . $resim->extension();

            if ($resim->isValid()) {

                $hedef_klasor = 'uploads/dosyalar/klas_slid';
                $dosya_yolu = $hedef_klasor . '/' . $dosya_adi;
                $resim->move($hedef_klasor, $dosya_adi);
                $slider->slider_resmi = $dosya_yolu;


            }
        }
        $slider->save();




        if ($slider) {
            alert()
                ->success('Başarılı', 'İşlem Başarılı')

                ->autoClose(1000);
            return back();


        } else {
            alert()
                ->error('Hata', 'İşlem Başarısız')

                ->autoClose(1000);
            return back();

        }
    }


    public function destroy($id)
    {
        $fotograf = Slider::find($id);
        $sildosya = $fotograf->slider_resmi;
        File::delete($sildosya);
        $sil = Slider::destroy($id);
        if ($sil) {
            alert()
                ->success('Başarılı', 'İşlem Başarılı')

                ->autoClose(1000);
            return back();


        } else {
            alert()
                ->error('Hata', 'İşlem Başarısız')

                ->autoClose(1000);
            return back();

        }
    }


}
