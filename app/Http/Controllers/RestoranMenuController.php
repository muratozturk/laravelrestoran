<?php

namespace App\Http\Controllers;

use App\RestoranMenu;
use App\RestoranmenuKategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RestoranMenuController extends Controller
{

    public function index()
    {
        $menuler = RestoranMenu::all();
        return view('admin.restoranmenu.index',compact('menuler'));
    }

    public function create()
    {
        $kategoriler = RestoranmenuKategori::all();
        return view('admin.restoranmenu.create',compact('kategoriler'));
    }


    public function store(Request $request)
    {
        $this->validate(request(), array(

        'menuadi' => 'required',
        'fiyat' => 'required',
        'kategori' => 'required',
                                        ));
        $menu = new RestoranMenu();
        $menu->menuadi = request('menuadi');
        $menu->menuaciklama = request('menuaciklama');
        $menu->kategori = request('kategori');
        $menu->fiyat = request('fiyat');
        $menu->slug = str_slug (request('menuadi'));

        if (request()->hasFile('resim')) {

            $validator = Validator::make($request->all(), [
                'resim' => 'image|mimes:jpeg,png,jpg,gif,svg|max:512',
            ]);
            if (!$validator->passes()) {
                alert()
                    ->error('Foto Yüklenemedi', 'Foto Dosya Boyutu Çok Büyük')
                    ->autoClose(1000);
                return back();
            }


            if (request()->hasFile('resim')) {

                $this->validate(request(), array('resim' => 'image|mimes:png,jpg,jpeg,gif|max:2048'));

                $resim = request()->file('resim');
                $dosya_adi = time() . '.' . $resim->extension();

                if ($resim->isValid()) {

                    $hedef_klasor = 'uploads/dosyalar/menuler';
                    $dosya_yolu = $hedef_klasor . '/' . $dosya_adi;
                    $resim->move($hedef_klasor, $dosya_adi);
                    $menu->resim = $dosya_yolu;
                }
            }
        }

        $menu->save();

        if ($menu) {
            alert()
                ->success('Başarılı', 'Menü Kaydedildi')
                ->autoClose(1000);
            return back();


        } else {
            alert()
                ->error('Hata', 'Kaydedilmedi')
                ->autoClose(1000);
            return back();

        }
    }


    public function edit($id)
    {
        $menu = RestoranMenu::find($id);
        $kategoriler = RestoranmenuKategori::where('id','!=', $menu->kategori)->get();
        return view('admin.restoranmenu.edit', compact('menu','kategoriler'));
    }


    public function update(Request $request, $id)
    {
        $this->validate(request(), array(

            'menuadi' => 'required',
            'fiyat' => 'required',
            'kategori' => 'required',
        ));
        $menu = RestoranMenu::find($id);
        $menu->menuadi = request('menuadi');
        $menu->menuaciklama = request('menuaciklama');
        $menu->kategori = request('kategori');
        $menu->fiyat = request('fiyat');
        $menu->slug = str_slug (request('menuadi'));

        if (request()->hasFile('resim')) {

            $validator = Validator::make($request->all(), [
                'resim' => 'image|mimes:jpeg,png,jpg,gif,svg|max:512',
            ]);
            if (!$validator->passes()) {
                alert()
                    ->error('Foto Yüklenemedi', 'Foto Dosya Boyutu Çok Büyük')
                    ->autoClose(1000);
                return back();
            }


            if (request()->hasFile('resim')) {

                $this->validate(request(), array('resim' => 'image|mimes:png,jpg,jpeg,gif|max:2048'));

                $resim = request()->file('resim');
                $dosya_adi = time() . '.' . $resim->extension();

                if ($resim->isValid()) {

                    $hedef_klasor = 'uploads/dosyalar/menuler';
                    $dosya_yolu = $hedef_klasor . '/' . $dosya_adi;
                    $resim->move($hedef_klasor, $dosya_adi);
                    $menu->resim = $dosya_yolu;
                }
            }
        }

        $menu->save();

        if ($menu) {
            alert()
                ->success('Başarılı', 'Menü Güncellendi')
                ->autoClose(1000);
            return back();


        } else {
            alert()
                ->error('Hata', 'Güncellenmedi')
                ->autoClose(1000);
            return back();

        }
    }


    public function destroy($id)
    {
        $sil = RestoranMenu::destroy($id);
        if ($sil) {
            alert()
                ->success('Başarılı','Menü Silindi')
                ->autoClose(1000);
            return back();


        }else {
            alert()
                ->error('Hata','Menü Silinmedi')
                ->autoClose(1000);
            return back();

        }
    }
}
