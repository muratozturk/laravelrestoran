<?php

namespace App\Http\Controllers;

use App\Ayar;
use App\Galeri;
use App\Hizmet;
use App\Mail\iletisimformu;
use App\RestoranMenu;
use App\Yorumlar;
use Illuminate\Http\Request;
use App\User;
use App\Yazi;
use App\Sayfa;
use App\Referans;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;


class YonetimController extends Controller
{

    public function index()
    {
        //$hizmetler = Hizmet::orderby('created_at','desc')->take(5)->get();
        $lastnews = Yazi::orderby('created_at','desc')->take(10)->get();
        $yorumlar = Yorumlar::orderby('created_at','desc')->take(10)->get();
        $menuler = RestoranMenu::orderby('created_at','desc')->take(10)->get();

        $sayfacount = Sayfa::all()->count();
        $habercount = Yazi::all()->count();
        $restoranmenucount = RestoranMenu::all()->count();
        /*$hizmetcount = Hizmet::all()->count();*/
        $galericount = Galeri::all()->count();

        return view('admin.index',
            compact('sayfacount','habercount','restoranmenucount','hizmetler','lastnews','yorumlar','menuler','galericount'));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }

   public function kullanicilar()
   {

       $kullanicilar = User::all();
       return view('admin.kullanicilar.index', compact('kullanicilar'));
   }

    public function kullaniciekle(){

        return view('admin.kullanicilar.create');
    }

    public function kullanicikayit(request $request)
    {

        $this->validate(request(), array(
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'password_confirmation' => 'required',

        ));

        $kullanici = new User();
        $kullanici->name = request('name');
        $kullanici->email = request('email');
        $kullanici->yetki = request('yetki');

        if (request('password') != request('password_confirmation')) {

            alert()
                ->error('Hata', 'Şİfreler Eşleşmiyor')
                ->showConfirmButton()
                ->autoClose(1000);
            return back();
        } else {
            $kullanici->password = Hash::make(request('password'));
        }

        $kullanici->save();

        if ($kullanici) {

           alert()
           ->success('Başarılı', 'İşlem Başarılı')
           ->autoClose(1000);
           return back();
             } else {
           alert()
            ->error('Hata', 'İşlem Başarısız')
            ->autoClose(1000);
            return back();

            }
    }
    public function kullaniciduzenle($id) {

        $kullanici = User::find($id);
        return view('admin.kullanicilar.edit',compact('kullanici'));



    }

    public function kullaniciguncelle($id){

        $this->validate(request(),array(
            'name'=>'required',
            'email'=>'required',

        ));


        $kullanici = User::find($id);
        $kullanici->name = request('name');
        $kullanici->email = request('email');
        $kullanici->yetki = request('yetki');
        $kullanici->avatar = request('avatar');



        if(request('password') != request('password_confirmation')) {

            alert()
                ->error('Hata','Şİfreler Eşleşmiyor')
               ->autoClose(1000);
            return back();
        }
        else{
            $kullanici->password = Hash::make(request('password'));
        }

        $kullanici->save();
        if($kullanici){
            alert()
                ->success('Başarılı','İşlem Başarılı')
                ->autoClose(1000);
            return back();


        }else {
            alert()
                ->error('Hata','İşlem Başarısız')
                ->autoClose(1000);
            return back();

        }
    }
    public function kullanicisil($id) {

        if(Auth::id()==$id){
            alert()
                ->error('Hata', 'Kendini Silemessin Mayk')
                ->autoClose(2000);
            return back();

        }else {

            $sil = User::find($id)->delete();

            if ($sil) {
                alert()
                    ->success('Başarılı', 'İşlem Başarılı')
                    ->autoClose(1000);
                return back();


            } else {
                alert()
                    ->error('Hata', 'İşlem Başarısız')
                    ->autoClose(1000);
                return back();

            }
        }
    }
    public function cikis() {

        auth()->logout();
        return redirect('/');
    }
    public function iletisim() {
        return view('admin.iletisimformu');
    }

    public function iletisimformgondeer(request $request)
    {
        $this->validate(request(), array(

            'ad' => 'required',
            'email' => 'required',
            'konu' => 'required',

        ));

        $ayarlar = Ayar::find(1);
        $siteadi = $ayarlar->site_adi;
        $siteemail = $ayarlar->email;

        $bilgiler = array(

            'ad' => request('ad'),
            'email' => request('email'),
            'konu' => request('konu'),
            'sitebaslik' => $siteadi,
            'siteemail' => $siteemail,


        );

        Mail::to($siteemail)->send(new iletisimformu($bilgiler));

        alert()
            ->success('Gönderildi', 'Formunuz Gönderildi')
            ->autoClose(1000);
        return back();

        }


}

