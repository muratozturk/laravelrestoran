<?php

namespace App\Http\Controllers;

use App\Kategori;
use Illuminate\Http\Request;
use App\Yazi;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class YaziController extends Controller
{

    public function index()
    {
        $yazilar = Yazi::all();
        return view('admin.yazilar.index',compact('yazilar'));

    }


    public function create()
    {
        $kategoriler = Kategori::all();
        return view('admin.yazilar.create',compact('kategoriler'));

    }

    public function store(Request $request)
    {


        $this->validate(request(), array(

            'baslik' => 'required',
            'icerik' => 'required',
            'kategori' => 'required',


        ));

        $yazi = new Yazi();
        $yazi->baslik = request('baslik');
        $yazi->icerik = request('icerik');
        $yazi->user_id = Auth::user()->id;
        $yazi->kategori_id = request('kategori');
        $yazi->slug = str_slug (request('baslik'));

        if (request()->hasFile('resim')) {

            $validator = Validator::make($request->all(), [
                'resim' => 'image|mimes:jpeg,png,jpg,gif,svg|max:512',
            ]);
            if (!$validator->passes()) {
                alert()
                    ->error('Foto Yüklenemedi', 'Foto Dosya Boyutu Çok Büyük')
                    ->autoClose(1000);
                return back();
            }


            if (request()->hasFile('resim')) {

                $this->validate(request(), array('resim' => 'image|mimes:png,jpg,jpeg,gif|max:2048'));

                $resim = request()->file('resim');
                $dosya_adi = time() . '.' . $resim->extension();

                if ($resim->isValid()) {

                    $hedef_klasor = 'uploads/dosyalar/klas_yazi';
                    $dosya_yolu = $hedef_klasor . '/' . $dosya_adi;
                    $resim->move($hedef_klasor, $dosya_adi);
                    $yazi->resim = $dosya_yolu;
                }


            }

        }
            $yazi->save();
            if ($yazi) {
                alert()
                    ->success('Başarılı', 'İşlem Başarılı')
                    ->autoClose(1000);
                return back();


            } else {
                alert()
                    ->error('Hata', 'İşlem Başarısız')
                    ->autoClose(1000);
                return back();

            }

    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $yazi = Yazi::find($id);
        $kategoriler = Kategori::where('id','!=', $yazi->kategori_id)->get();
        return view('admin.yazilar.edit', compact('yazi','kategoriler'));
    }

    public function update(Request $request, $id)
    {
        $this->validate(request(), array(

            'baslik' => 'required',
            'icerik' => 'required',
            'kategori' => 'required',


        ));

        $yazi = Yazi::find($id);
        //$kullanici = $yazi->user_id;
        $yazi->baslik = request('baslik');
        $yazi->icerik = request('icerik');
        //$yazi->user_id = $kullanici;
        $yazi->kategori_id = request('kategori');
        $yazi->slug = str_slug (request('baslik'));





            if (request()->hasFile('resim')) {

                $this->validate(request(), array('resim' => 'image|mimes:png,jpg,jpeg,gif|max:2048'));
                $resim = request()->file('resim');
                $dosya_adi = time() . '.' . $resim->extension();

                if ($resim->isValid()) {

                    $hedef_klasor = 'uploads/dosyalar/klas_yazi';
                    $dosya_yolu = $hedef_klasor . '/' . $dosya_adi;
                    $resim->move($hedef_klasor, $dosya_adi);
                    $yazi->resim = $dosya_yolu;

                }
            }


            $yazi->save();
            if ($yazi) {
                alert()
                    ->success('Başarılı', 'İçerik Güncellendi')
                    ->autoClose(1000);
                return back();


            } else {
                alert()
                    ->error('Hata', 'İşlem Başarısız')
                    ->autoClose(1000);
                return back();

            }
        }



    public function destroy($id)
    {
        $fotograf = Yazi::find($id);
        $sildosya = $fotograf->resim;
        File::delete($sildosya);
        $sil = Yazi::destroy($id);
        if ($sil) {
            alert()
                ->success('Başarılı','İşlem Başarılı')
                ->autoClose(1000);
            return back();


        }else {
            alert()
                ->error('Hata','İşlem Başarısız')
                ->autoClose(1000);
            return back();

        }

        }

}
