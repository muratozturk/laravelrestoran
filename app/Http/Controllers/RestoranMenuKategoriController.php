<?php

namespace App\Http\Controllers;

use App\RestoranMenu;
use App\RestoranmenuKategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class RestoranmenuKategoriController extends Controller
{

    public function index()
    {
        $kategoriler = RestoranmenuKategori::all();
        return view('admin.restoranmenukategori.index',compact('kategoriler'));
    }

    public function create()
    {
        return view ('admin.restoranmenukategori.create');
    }

    public function store(Request $request)
    {
        $kategori = new RestoranmenuKategori();
        $kategori->restoranmenu_kategori = request('kategori_baslik');
        $kategori->restoranmenu_kategori_aciklama = request('kategori_aciklama');
        $kategori->slug = str_slug(request('kategori_baslik'));

        if (request()->hasFile('resim')) {

            $validator = Validator::make($request->all(), [
                'resim' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            ]);
            if (!$validator->passes()) {
                alert()
                    ->error('Foto Yüklenemedi', 'Foto Dosya Boyutu Çok Büyük')

                    ->autoClose(1000);
                return back();
            }

            $resim = request()->file('resim');
            $dosya_adi = 'kategori' . '-' . time() . '.' . $resim->extension();

            if ($resim->isValid()) {

                $hedef_klasor = 'uploads/dosyalar/restorankategori';
                $dosya_yolu = $hedef_klasor . '/' . $dosya_adi;
                $resim->move($hedef_klasor, $dosya_adi);
                $kategori->fotograf = $dosya_yolu;


            }
        }


        $kategori->save();
        if($kategori){

            alert()
                ->success('Başarılı', 'Kategori Eklendi')
                ->autoClose(2000);
            return back();
        }else{
            alert()
                ->error('Başarısız', 'Kategori Eklenemedi')
                ->autoClose(2000);
            return back();
        }
    }

    public function edit($id)
    {
        $kategori = RestoranmenuKategori::find($id);

        return view('admin.restoranmenukategori.edit', compact('kategori'));
    }


    public function update(Request $request, $id)
    {
        $kategori = RestoranmenuKategori::find($id);
        $kategori->restoranmenu_kategori = request('restoranmenu_kategori');
        $kategori->restoranmenu_kategori_aciklama = request('restoranmenu_kategori_aciklama');
        $kategori->slug = str_slug(request('restoranmenu_kategori'));


        if (request()->hasFile('resim')) {

            $validator = Validator::make($request->all(), [
                'resim' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            ]);
            if (!$validator->passes()) {
                alert()
                    ->error('Foto Yüklenemedi', 'Foto Dosya Boyutu Çok Büyük')

                    ->autoClose(1000);
                return back();
            }

            $resim = request()->file('resim');
            $dosya_adi = 'kategori' . '-' . time() . '.' . $resim->extension();

            if ($resim->isValid()) {

                $hedef_klasor = 'uploads/dosyalar/restorankategori';
                $dosya_yolu = $hedef_klasor . '/' . $dosya_adi;
                $resim->move($hedef_klasor, $dosya_adi);
                $kategori->fotograf = $dosya_yolu;


            }
        }
        $kategori->save();
        if($kategori){

            alert()
                ->success('Başarılı', 'Kategori Güncellendi')
                ->autoClose(2000);
            return back();
        }else{
            alert()
                ->error('Başarısız', 'Kategori Güncellenmedi')
                ->autoClose(2000);
            return back();
        }
    }


    public function destroy($id)
    {

        $menukategori = RestoranMenu::where('kategori','=',$id)->count() > 0;
        if($menukategori){
            alert()
                ->error('Silinemez', 'Kategoriye Ait Menü Bulunmakta')
                ->autoClose(1000);
            return back();
        }else {
            $fotograf = RestoranmenuKategori::find($id);
            $sildosya = $fotograf->fotograf;
            File::delete($sildosya);
            RestoranmenuKategori::destroy($id);

            alert()
                ->success('Silindi', 'Kategori Silindi')
                ->autoClose(1000);
            return back();
        }
    }
}
