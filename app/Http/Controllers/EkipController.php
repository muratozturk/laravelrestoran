<?php

namespace App\Http\Controllers;

use App\Ekip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class EkipController extends Controller
{

    public function index()
    {
        $ekip = Ekip::all();
        return view('admin.ekip.index', compact('ekip'));
    }

    public function create()
    {
        return view('admin.ekip.create');
    }

    public function store(Request $request)
    {
        $this->validate(request(), array(
            'ad' => 'required',
            'gorevi' => 'required',
        ));

        $ekip = new Ekip();
        $ekip->ad = request('ad');
        $ekip->gorevi = request('gorevi');
        $ekip->slug = str_slug (request('ad'));

        if (request()->hasFile('resim')) {

            $validator = Validator::make($request->all(), [
                'resim' => 'image|mimes:jpeg,png,jpg,gif,svg|max:512',
            ]);
            if (!$validator->passes()) {
                alert()
                    ->error('Foto Yüklenemedi', 'Foto Dosya Boyutu Çok Büyük')
                    ->autoClose(1000);
                return back();
            }

            $resim = request()->file('resim');
            $dosya_adi = 'ekip' . '-' . time() . '.' . $resim->extension();

            if ($resim->isValid()) {

                $hedef_klasor = 'uploads/dosyalar/ekip';
                $dosya_yolu = $hedef_klasor . '/' . $dosya_adi;
                $resim->move($hedef_klasor, $dosya_adi);
                $ekip->resim = $dosya_yolu;


            }
        }
        $ekip->save();
        if ($ekip) {
            alert()
                ->success('Başarılı', 'Ekip Üyesi Kaydedildi')
                ->autoClose(1000);
            return back();


        } else {
            alert()
                ->error('Hata', 'İşlem Başarısız')
                ->autoClose(1000);
            return back();

        }

    }


    public function edit($id)
    {
        $ekip = Ekip::find($id);
        return view('admin.ekip.edit',compact('ekip'));
    }

    public function update(Request $request, $id)
    {
        $this->validate(request(), array(
            'ad' => 'required',
            'gorevi' => 'required',
        ));

        $ekip = Ekip::find($id);
        $ekip->ad = request('ad');
        $ekip->gorevi = request('gorevi');

        if (request()->hasFile('resim')) {

            $validator = Validator::make($request->all(), [
                'resim' => 'image|mimes:jpeg,png,jpg,gif,svg|max:512',
            ]);
            if (!$validator->passes()) {
                alert()
                    ->error('Foto Yüklenemedi', 'Foto Dosya Boyutu Çok Büyük')
                    ->autoClose(1000);
                return back();
            }

            $resim = request()->file('hizmet_one_cikan_foto');
            $dosya_adi = 'ekip' . '-' . time() . '.' . $resim->extension();

            if ($resim->isValid()) {

                $hedef_klasor = 'uploads/dosyalar/ekip';
                $dosya_yolu = $hedef_klasor . '/' . $dosya_adi;
                $resim->move($hedef_klasor, $dosya_adi);
                $ekip->resim = $dosya_yolu;


            }
        }
        $ekip->save();
        if ($ekip) {
            alert()
                ->success('Başarılı', 'Ekip Üyesi Kaydedildi')
                ->autoClose(1000);
            return back();


        } else {
            alert()
                ->error('Hata', 'İşlem Başarısız')
                ->autoClose(1000);
            return back();

        }
    }

    public function destroy($id)
    {
        $fotograf = Ekip::find($id);
        $sildosya = $fotograf->resim;
        File::delete($sildosya);
        $sil = Ekip::destroy($id);
        if ($sil) {
            alert()
                ->success('Başarılı', 'Silindi')
                ->autoClose(1000);
            return back();


        } else {
            alert()
                ->error('Hata', 'İşlem Başarısız')
                ->autoClose(1000);
            return back();

        }
    }
}
