<?php

namespace App\Http\Controllers;

use App\AnasayfaAyar;
use Illuminate\Http\Request;

class AnasayfaAyarController extends Controller
{

    public function index()
    {
        $anasayfaayar = AnasayfaAyar::find(1);
        return view('admin.ayarlar.anasayfaayar',compact('anasayfaayar'));
    }

    public function update(Request $request, $id)
    {
        $anasayfaayar = AnasayfaAyar::find(1);
        $anasayfaayar->metin = request('metin');
        $anasayfaayar->yorumlar = request('yorumlar');
        $anasayfaayar->galeri = request('galeri');
        $anasayfaayar->blog = request('blog');
        $anasayfaayar->menu = request('menu');
        $anasayfaayar->save();
        if($anasayfaayar){
            alert()
                ->success('Başarılı','İşlem Başarılı')
                ->autoClose(1000);
            session()->put('success','Güncellendi.');

            return back();

        }else {
            alert()
                ->error('Hata','İşlem Başarısız')
                ->autoClose(1000);
            return back();

        }
    }

}
