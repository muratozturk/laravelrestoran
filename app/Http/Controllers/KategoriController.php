<?php

namespace App\Http\Controllers;

use App\Kategori;
use App\Yazi;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategoriler = Kategori::all();
        return view('admin.kategoriler.index',compact('kategoriler'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view ('admin.kategoriler.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        $kategori = new Kategori();
        $kategori->kategori_baslik = request('kategori_baslik');
        $kategori->kategori_aciklama = request('kategori_aciklama');
        $kategori->kategori_slug = str_slug(request('kategori_baslik'));

        $kategori->save();
        if($kategori){

            alert()
                ->success('Başarılı', 'Kategori Eklendi')
                ->autoClose(2000);
            return back();
        }else{
            alert()
                ->error('Başarısız', 'Kategori Eklenemedi')
                ->autoClose(2000);
            return back();
        }
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Kategori::find($id);

        return view('admin.kategoriler.edit', compact('kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kategori = Kategori::find($id);
        $kategori->kategori_baslik = request('kategori_baslik');
        $kategori->kategori_aciklama = request('kategori_aciklama');
        $kategori->kategori_slug = str_slug(request('kategori_baslik'));

        $kategori->save();
        if($kategori){

            alert()
                ->success('Başarılı', 'Kategori Güncellendi')
                ->autoClose(1000);
            return back();
        }else{
            alert()
                ->error('Başarısız', 'Kategori Güncellenemedi')
                ->autoClose(1000);
            return back();
        }
    }

    public function destroy($id)
    {
        $icerik = Yazi::where('kategori_id','=',$id)->count() > 0;
        if($icerik){
            alert()
                ->error('Silinemez', 'Kategoriye Ait Yazı Bulunmakta')
                ->autoClose(1000);
            return back();
        }else {
            Kategori::destroy($id);

            alert()
                ->success('Silindi', 'Kategori Silindi')
                ->autoClose(1000);
            return back();
        }

    }
}
