<?php

namespace App\Http\Controllers;

use App\Referans;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class ReferansController extends Controller
{
    public function index()
    {
        $referanslar = Referans::all();
        return view('admin.referanslar.index', compact('referanslar'));
    }


    public function create()
    {
        return view('admin.referanslar.create');
    }

    public function store(Request $request)
    {
        $this->validate(request(), array(
            'referans_adi' => 'required',
            'firma_adi' => 'required',

        ));

        $referans = Referans::create($request->all());
        if (request()->hasFile('referans_gorseli')) {

            $validator = Validator::make($request->all(), [
                'referans_gorseli' => 'image|mimes:jpeg,png,jpg,gif,svg|max:512',
            ]);
            if (!$validator->passes()) {
                alert()
                    ->error('Foto Yüklenemedi', 'Foto Dosya Boyutu Çok Büyük')
                    ->showConfirmButton()
                    ->autoClose(2000);
                return back();
            }

            $resim = request()->file('referans_gorseli');
            $dosya_adi = 'referans_gorseli' . '-' . time() . '.' . $resim->extension();

                if ($resim->isValid()) {

                    $hedef_klasor = 'uploads/dosyalar/klas_refr';
                    $dosya_yolu = $hedef_klasor . '/' . $dosya_adi;
                    $resim->move($hedef_klasor, $dosya_adi);
                    $referans->referans_gorseli = $dosya_yolu;
                }
            }
        $referans->save();



        if ($referans) {
            alert()
                ->success('Başarılı', 'İşlem Başarılı')
                ->autoClose(1000);
            return back();


        } else {
            alert()
                ->error('Hata', 'İşlem Başarısız')
                ->autoClose(1000);
            return back();

        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $referans = Referans::find($id);
        return view('admin.referanslar.edit', compact('referans'));
    }


    public function update(Request $request, $id)
    {
        $this->validate(request(), array(
            'referans_adi' => 'required',
            'firma_adi' => 'required',

        ));
        $referans = Referans::find($id);
        $bilgiler = $request->all();
        $referans->update($bilgiler);
        if (request()->hasFile('referans_gorseli')) {

            $validator = Validator::make($request->all(), [
                'referans_gorseli' => 'image|mimes:jpeg,png,jpg,gif,svg|max:512',
            ]);
            if (!$validator->passes()) {
                alert()
                    ->error('Foto Yüklenemedi', 'Foto Dosya Boyutu Çok Büyük')
                    ->autoClose(1000);
                return back();
            }

            $resim = request()->file('referans_gorseli');
            $dosya_adi = 'referans_gorseli' . '-' . time() . '.' . $resim->extension();

            if ($resim->isValid()) {

                $hedef_klasor = 'uploads/dosyalar/klas_refr';
                $dosya_yolu = $hedef_klasor . '/' . $dosya_adi;
                $resim->move($hedef_klasor, $dosya_adi);
                $referans->referans_gorseli = $dosya_yolu;
            }
        }
        $referans->save();


        if ($referans) {
            alert()
                ->success('Başarılı', 'İşlem Başarılı')
                ->autoClose(1000);
            return back();


        } else {
            alert()
                ->error('Hata', 'İşlem Başarısız')
                ->autoClose(1000);
            return back();

        }
    }

    public function destroy($id)
    {
        $fotograf = Referans::find($id);
        $sildosya = $fotograf->referans_gorseli;
        File::delete($sildosya);
        $sil = Referans::destroy($id);
        if ($sil) {
            alert()
                ->success('Başarılı', 'İşlem Başarılı')
                ->autoClose(1000);
            return back();


        } else {
            alert()
                ->error('Hata', 'İşlem Başarısız')
                ->autoClose(1000);
            return back();

        }
    }


}
