<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestoranmenuKategori extends Model
{
    protected $table = 'restoranmenu_kategori';
    protected $guarded = [];

    public function menuleri() {

        return $this->hasMany('App\RestoranMenu', 'kategori');
    }
}
