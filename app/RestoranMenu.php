<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestoranMenu extends Model
{
    protected $table = 'restoranmenu';
    protected $guarded = [];

    public function kategorisi() {

        return $this->belongsTo('App\RestoranmenuKategori', 'kategori');
    }
}
