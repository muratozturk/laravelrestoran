<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Yorumlar extends Model
{
    protected $table = 'yorumlar';
    protected $guarded = [];
}
